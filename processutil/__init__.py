import psutil

def getPId(name):
    '''
    获取程序pid
    :param name: 程序名
    :return: pid 如果没有返回None
    '''
    if name and isinstance(name, str):
        for proc in psutil.process_iter(attrs=['pid', 'name']):
            processdict=proc.info
            tempname = processdict.get('name', None)
            if tempname and name == tempname:
                return processdict.get('pid', None)
    return None


def ProcessIsExit(name):
    '''
    判断进程是否存在
    :param name: str or int
    :return: boolean
    '''
    # 直接输入了pid
    if isinstance(name, int):
        return psutil.pid_exists(name)
    elif isinstance(name, str):
        pid = getPId(name)
        if pid:
            return psutil.pid_exists(pid)
    else:
        return False

if __name__=="__main__":
    ProcessIsExit('java')
