import json
import os
import datetime
from sqlalchemy import and_

from dbmanager.sqllite import SqliteSession, DBXmlUpload


def getFilePathList(rootpath,now):
    '''
        根据日期及根路径获取文件夹路径
    '''
    path = os.path.join(rootpath, str(now.year), '0'+str(now.month) if len(str(now.month))==1 else str(now.month) , str(now.day), 'XML/')
    filelist = os.listdir(path)
    return map(lambda x: path + x, filelist)


def analizeFileIsUpload(now, filelist):
    '''
    判断是否已经解析过并上传了
    :param list:
    :return:
    '''
    uploadlist = []
    session = SqliteSession()
    templist = session.query(DBXmlUpload).filter(and_(DBXmlUpload.date == now)).all()
    if len(templist) > 0:
        templist = map(lambda x: json.loads(x.filelist), templist)
        for i in templist:
            uploadlist = uploadlist + i
    # 如果没有全部返回
    if len(uploadlist) == 0:
        return filelist
    else:
        filelist=set(filelist).difference(set(uploadlist))
        return filelist
