import yaml

config = None


def loadConfig(filepath):
    with open(filepath, 'r') as file:
        config = yaml.load(file)
    return config