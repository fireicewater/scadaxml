from functools import wraps


def sessionwrapper(sessionfactory):
    '''
    事务自动头提交 类方法级别
    :param func:
    :return:
    '''

    def decorate(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            session = sessionfactory()
            try:
                args = list(args)
                args.append(session)
                func(*args, **kwargs)
                session.commit()
            except  Exception as e:
                print(e)
                session.rollback()
            finally:
                session.close()

        return wrapper

    return decorate
