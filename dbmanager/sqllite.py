
from sqlalchemy import create_engine, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.orm import sessionmaker

engine = create_engine("sqlite:///sqlitedb/scada.db", encoding="utf-8", echo=True)

Base = declarative_base()


class DBXmlUpload(Base):
    '''
    数据库游标存储
    '''
    __tablename__ = 'xmlupload'
    id = Column(Integer, primary_key=True)
    # sqlserver主键id游标
    filelist = Column(String)
    # 修改时间
    date = Column(Date)


class DBProcesstime(Base):
    '''
    记录程序时间
    '''
    __tablename__ = 'processtime'

    id = Column(Integer, primary_key=True)
    # 程序名
    name = Column(String(length=50), )
    # 是否开启
    isOpen = Column(Boolean)
    # 时间
    date = Column(DateTime)


SqliteSession = sessionmaker(bind=engine)

if __name__=='__main__':
    Base.metadata.create_all(engine)
